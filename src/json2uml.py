#! python

import json
import os
from pygraphviz import AGraph
import uuid

class JSONError(Exception):
	def __init__(self, message):
		self.message = message
	def __repr__(self):
		return self.__class__.__name__
	def __str__(self):
		return repr(self) + ': ' + self.message

JSON_SIMPLE_TYPES = ['string', 'number', 'integer', 'boolean']

def parse_object(content):
	# @type graph AGraph
	global graph
	
	if ('properties' not in content) or not isinstance(content['properties'], dict):
		raise JSONError('Missing properties for ' + content['title'])
	
	label = '{' + content['title'] + '|'
	
	for property, info in content['properties'].items():
		if not isinstance(info, dict) or ('type' not in info):
			raise JSONError('Malformed property ' + property)
		
		if info['type'] == 'object':
			continue
		
		label += property
		if info['type'] in JSON_SIMPLE_TYPES:
			label += ' : ' + info['type']
		elif info['type'] == 'array':
			label += ' : '
			if ('items' in info) and ('type' in info['items']):
				label += info['items']['type']
			label += '[]'
		label += '\l'
	
	label += '}'
	
	node_id = uuid.uuid4()
	graph.add_node(node_id, label=label, shape='record')
	
	for property, info in content['properties'].items():
		
		while (info['type'] == 'array') and ('items' in info):
				info = info['items']
		
		if info['type'] == 'object':
			if 'title' not in info:
				info['title'] = property
			child_id = parse_object(info)
			
			property = property+property
			graph.add_edge(child_id, node_id)
	
	return node_id

# here comes the main program
if __name__ == "__main__":
	# @type input file
	input = open('../examples/test.json', 'r')

	try:
		content = json.load(input)

		if ('type' in content) and (content['type'] != 'object'):
				raise JSONError('Root element has to be of type object!')
			
		if 'title' not in content:
			content['title'] = os.path.basename(input.name)

		global graph
		graph = AGraph(strict=False, directed=True, overlap=False, splines=True)
		
		graph.edge_attr['arrowhead'] = 'diamond'
		graph.edge_attr['minlen'] = 1

		parse_object(content)
		
		print graph
		
		graph.layout(prog='dot')
		graph.draw('output.png')
	except JSONError, e:
		print str(e)
	finally:
		input.close()
